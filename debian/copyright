Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: network
Upstream-Contact: Carter T. Butts <buttsc@uci.edu>
Source: https://cran.r-project.org/package=network

Files: *
Copyright: 2005-2018 Carter T. Butts,
 David Hunter,
 Mark Handcock,
 Skye Bender-deMoll,
 Jeffrey Horner
 2003-2015 Statnet Commons
License: GPL-2+

Files: R/as.edgelist.R
Copyright: 2003-2015 Statnet Commons
License: GPL-3
Comment:
  This software is distributed under the GPL-3 license.  It is free,
  open source, and has the attribution requirements (GPL Section 7) at
  http://statnet.org/attribution
  .
  [remaining text is from http://statnetproject.org/attribution ]
  What does this mean?
  .
  If you are modifying statnet or adopting any source code from statnet for use
  in another application, you must ensure that the copyright and attributions
  mentioned in the license above appear in the code of your modified version or
  application. These attributions must also appear when the package is loaded
  (e.g., via "library" or "require").
  .
  In addition, if you are using the statnet package or any of the component
  packages for research that will be published, we request that you acknowledge
  this with a citation. See http://statnet.org/citation.html.
  .
  License Information for the component packages (e.g., ergm)
  The licenses for these packages are similar to statnet. They can be found in
  the statnet package using the citation function, e.g.:
  .
  citation('ergm')

Files: inst/network.api/networkapi.c
 inst/network.api/networkapi.h
 src/Rinit.c
 src/access.c
 src/access.h
 src/constructors.c
 src/constructors.h
 src/layout.c
 src/layout.h
 src/utils.c
 src/utils.h
Copyright: Carter T. Butts <buttsc@uci.edu>
           Jeffrey Horner <jeffrey.horner@gmail.com>
License: GPL-2+

Files: debian/*
Copyright: 2019 Tilburg University https://tilburguniversity.edu/
           2020 Michael R. Crusoe <crusoe@debian.org>
Comment: author: Joost van Baal-Ilić
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of tqhe GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.
